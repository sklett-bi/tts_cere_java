package dialog.generation.tts.tts_cere;

import java.lang.reflect.Field;

// Copyright (c) 2010-2011 Cereproc Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions: 
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//
// BasicTts.java
//
// An example program for CereVoice Engine wrapped for Java,
// it plays audio directly.
//
// This version demonstrates use of a callback function,
// to allow low-latency synthesis and allow streaming of 
// input text.
//

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;

import com.cereproc.cerevoice_eng.CPRC_ABUF_TRANS_TYPE;
import com.cereproc.cerevoice_eng.CPRC_VOICE_LOAD_TYPE;
import com.cereproc.cerevoice_eng.SWIGTYPE_p_CPRCEN_engine;
import com.cereproc.cerevoice_eng.SWIGTYPE_p_CPRC_abuf;
import com.cereproc.cerevoice_eng.SWIGTYPE_p_CPRC_abuf_trans;
import com.cereproc.cerevoice_eng.TtsEngineCallback;
import com.cereproc.cerevoice_eng.cerevoice_eng;

import rsb.AbstractEventHandler;
import rsb.Event;
import rsb.Factory;
import rsb.Listener;
import rsb.converter.DefaultConverterRepository;
import rsb.converter.ProtocolBufferConverter;
import rst.tts.TextToSpeechInstructionType.TextToSpeechInstruction;

public class BasicTTS extends AbstractEventHandler {

	static {
	
		String path = "./share";
	    System.setProperty("java.library.path", path);

	    //set sys_paths to null so that java.library.path will be reevalueted next time it is needed
	    Field sysPathsField;
		try {
			sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
			sysPathsField.setAccessible(true);		    
			sysPathsField.set(null, null);			
		} catch (NoSuchFieldException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}    

		System.loadLibrary("cerevoice_eng");
	}

	public static void main(String[] argv) throws Exception {

		if (argv.length == 0) {
			System.out.println("usage: java -jar CereTTS_java.jar [scope]");
			System.out.println("continuing with default scope /example/informer");
		}

		final ProtocolBufferConverter<TextToSpeechInstruction> converter = new ProtocolBufferConverter<TextToSpeechInstruction>(
				TextToSpeechInstruction.getDefaultInstance());

		DefaultConverterRepository.getDefaultConverterRepository().addConverter(converter);

		final Factory factory = Factory.getInstance();
		String scope = "/example/informer";
		if (argv.length > 0) {
			scope = argv[0];
		}

		final Listener listener = factory.createListener(scope);
		listener.activate();
		try {
			listener.addHandler(new BasicTTS(), true);

			System.out.println("listener started on scope: " + scope);

			while (true) {
				Thread.sleep(10000);
			}
		} finally {
			listener.deactivate();
		}

	}


	
	@Override
	public void handleEvent(final Event event) {

		if (!(event.getData() instanceof TextToSpeechInstruction)) {
			System.out.println("received wrong rst-object type");
			return;
		}

		TextToSpeechInstruction t = (TextToSpeechInstruction) event.getData();

		System.out.println("synthesizing: " + t.getText());

		SWIGTYPE_p_CPRCEN_engine eng;
		int chan_handle, res;
		String rate_str;
		TtsEngineCallback cb;
		Float rate;

		// Load the voice
		System.out.println("INFO: creating CereVoice Engine");
		eng = cerevoice_eng.CPRCEN_engine_new();
		System.out.println("INFO: loading voice");
		res = cerevoice_eng.CPRCEN_engine_load_voice(eng, "gudrun.lic", "", "gudrun.voice", CPRC_VOICE_LOAD_TYPE.CPRC_VOICE_LOAD);

		if (res == 0) {
			System.out.println("ERROR: unable to load voice file '" + "gudrun.voice" + "', exiting");
			System.exit(-1);
		}

		// Open the default synthesis channel
		System.out.println("INFO: opening default channel");
		chan_handle = cerevoice_eng.CPRCEN_engine_open_default_channel(eng);
		if (chan_handle == 0) {
			System.out.println("ERROR: unable to open default channel, exiting");
			System.exit(-1);
		}

		// Get some information about the voice used on the channel, here the
		// sample rate
		rate_str = cerevoice_eng.CPRCEN_channel_get_voice_info(eng, chan_handle, "SAMPLE_RATE");
		System.out.println("INFO: channel has a sample rate of: " + rate_str);

		// Open an audio line for output
		rate = new Float(rate_str);
		audioline au = new audioline(rate.floatValue());
		// Set our callback class onto it
		cb = new MyTtsCallback(au.line());
		cb.SetCallback(eng, chan_handle);

		try {
			cerevoice_eng.CPRCEN_engine_channel_speak(eng, chan_handle, t.getText() + "\n", t.getText().getBytes().length + 1, 0);
			// Flush engine
			cerevoice_eng.CPRCEN_engine_channel_speak(eng, chan_handle, "", 0, 1);
		} catch (Exception e) {
			System.err.println("ERROR: " + e.getMessage());
		}
		// Flush any remaining audio
		au.flush();
		cb.ClearCallback(eng, chan_handle);
		// Close the channel
		cerevoice_eng.CPRCEN_engine_channel_close(eng, chan_handle);
		// Delete the engine
		cerevoice_eng.CPRCEN_engine_delete(eng);

	}

}	

// In CereVoice, a callback can be specified to receive data every
// time a phrase has been synthesised. The user application can
// process that data safely, as the engine does not continue until the
// user returns from the callback. Ideally, the processing done in
// the callback itself should be kept simple, such as sending audio
// data to the audio player, or sending timed phonetic information to
// a seperate thread for lip animation on a talking head.
class MyTtsCallback extends TtsEngineCallback {
	private SourceDataLine line;

	// Initialise with an audio line for playback
	public MyTtsCallback(SourceDataLine line) {
		this.line = line;
	}

	// Process the data in the callback. Here we convert audio from
	// shorts to bytes to play in Java. The callback function
	// receives a CereVoice audio buffer object, extracts the audio
	// and plays it.
	public void Callback(SWIGTYPE_p_CPRC_abuf abuf) {
		System.out.println("INFO: firing engine callback");
		int i, sz;
		// sz is the number of 16-bits samples
		System.out.println("INFO: checking audio size");
		sz = cerevoice_eng.CPRC_abuf_wav_sz(abuf);
		byte[] b = new byte[sz * 2];
		short s;
		SWIGTYPE_p_CPRC_abuf_trans trans;
		CPRC_ABUF_TRANS_TYPE transtype;
		float start, end;
		String name;

		// First process the transcription information and print it
		/* Process the transcription buffer items and print information. */
		for (i = 0; i < cerevoice_eng.CPRC_abuf_trans_sz(abuf); i++) {
			trans = cerevoice_eng.CPRC_abuf_get_trans(abuf, i);
			transtype = cerevoice_eng.CPRC_abuf_trans_type(trans);
			start = cerevoice_eng.CPRC_abuf_trans_start(trans);
			end = cerevoice_eng.CPRC_abuf_trans_end(trans);
			name = cerevoice_eng.CPRC_abuf_trans_name(trans);
			if (transtype == CPRC_ABUF_TRANS_TYPE.CPRC_ABUF_TRANS_PHONE) {
				System.err.printf("INFO: phoneme: %.3f %.3f %s\n", start, end, name);
			} else if (transtype == CPRC_ABUF_TRANS_TYPE.CPRC_ABUF_TRANS_WORD) {
				System.err.printf("INFO: word: %.3f %.3f %s\n", start, end, name);
			} else if (transtype == CPRC_ABUF_TRANS_TYPE.CPRC_ABUF_TRANS_MARK) {
				System.err.printf("INFO: marker: %.3f %.3f %s\n", start, end, name);
			} else if (transtype == CPRC_ABUF_TRANS_TYPE.CPRC_ABUF_TRANS_ERROR) {
				System.err.printf("ERROR: could not retrieve transcription at '%d'", i);
			}
		}

		// Now extract the audio, convert it, send it to java audio playback
		// This is not the most elegant way to do this conversion, but shows
		// how e.g. audio effects could be applied.
		for (i = 0; i < sz; i++) {
			// Sample at position i, a short
			s = cerevoice_eng.CPRC_abuf_wav(abuf, i);
			// The sample is written in Big Endian to the buffer
			b[i * 2] = (byte) ((s & 0xff00) >> 8);
			b[i * 2 + 1] = (byte) (s & 0x00ff);
		}
		// Send the audio data to the Java audio player
		line.write(b, 0, sz * 2);
	}
}

// Simple class to wrap a Java audio line
class audioline {
	private SourceDataLine line;
	private AudioFormat format;

	public audioline(float sampleRate) {
		format = getAudioFormat(sampleRate);
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
		// Obtain and open the line.
		try {
			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(format);
			line.start();
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	}

	public SourceDataLine line() {
		return line;
	}

	private static AudioFormat getAudioFormat(float sampleRate) {
		int sampleSizeInBits = 16;
		int channels = 1;
		boolean signed = true;
		boolean bigEndian = true;
		return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
	}

	public void flush() {
		line.drain();
		line.close();
	}
}
